import BannerBase from '../../js/base';
class Banner extends BannerBase {
	constructor(paths) {
		super(paths);
		this.pathData = [
            
			{ path: "#path_1", x: -33, y: -20},
			{ path: "#path_2", x: 27, y: 42},
			{ path: "#path_3", x: 29, y: 37}
		];
    }
    
    followAnimatingPath(tl, selector_pathToFollow, selector_followingElement){
        const motionPathData = MorphSVGPlugin.pathDataToBezier(selector_pathToFollow, { align: selector_followingElement, matrix: [0.28,0.00,0.00,0.28,0,0]});

        tl.to([selector_followingElement], 2.5, {bezier:{values:motionPathData, type:"cubic"}}, "-=2.5")
    } 

    getPathData(path){
        return MorphSVGPlugin.pathDataToBezier(path, { matrix: [0.28,0.00,0.00,0.28,0,0]});
    }

	step1(){

        this.dl_timeline.set([this.SELECTOR_HAND_IMAGE], {transformOrigin: "top left"})
        this.fadeInCopy(document.querySelectorAll('.banner__leadCopy1'));
             
        this.step2();   
        
    }

    step2(){

        this.timeline.to(['.banner__leadCopy1'], 1 , {delay: 0, opacity:0, onComplete:()=>{
            
        }}, "-=1")
		this.timeline.to('.banner__leadCopy2', 1, {delay: 0, opacity:1, onComplete:()=>{
            this.animateBg();
            
        }}, "-=1")
		this.timeline.to(['.banner__enbrelLogo'],1, { opacity:1, onComplete:()=>{
           
        }}, "-=1")
        this.animateELementsToJustAboveTheIsi('.banner__scissors', 35);
        this.animateELementsToJustAboveTheIsi('.banner__tape', 50, "-=1"); 

        this.timeline.to('.banner__scissors', 1, {width:95, x:-125, y:16, onComplete:()=>{
            
        }}, "-=1")
        this.timeline.to('.banner__tape', 1, {scale:.9, x:-100, y:-4, onComplete:()=>{
        }}, "-=1")

        this.timeline.to('#runningMan', 1, {opacity: 0, onComplete:()=>{
        }}, "-=1.5")

        this.timeline.to('.banner__enbrelLogo', 1, {scale: .4, x:'-95%', y:"-5%", onComplete:()=>{
           
        }}, "-=1")
        
		this.timeline.to('.banner__rulerAndChalk', 1, {opacity:1, onComplete:()=>{
        }}, "-=1")
		this.timeline.to(["#isi-wrapper", ".banner__piLinks"], 1, {x:0, y:0, onComplete:()=>{
        }})// isi comes up
        
        this.step3();
        
    }

    step3(){
        this.timeline.to(['.toHide'], 1, {opacity:0, display:"none", onComplete:()=>{
        }})
        this.timeline.to('.banner__button', 1, {opacity: 1, onComplete:()=>{

        }}, "-=1")
        this.timeline.to('.banner__rulerAndChalk', 1, {opacity:1, onComplete:()=>{
        }})

        
    }
}

const banner = new Banner();
