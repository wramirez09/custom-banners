import BannerBase from '../../js/base';

class Banner extends BannerBase {
	constructor() {
		super();
		this.pathData = [
            
			{ path: "#path_1", x: 21, y: 55},
			{ path: "#path_2", x: 24, y: 71},
			{ path: "#path_3", x: 24, y: 71}
		];
	}

	followAnimatingPath(tl, selector_pathToFollow, selector_followingElement){
        const motionPathData = MorphSVGPlugin.pathDataToBezier(selector_pathToFollow, { align: selector_followingElement, matrix: [0.60,0.00,0.00,0.60,0,0]});

        tl.to([selector_followingElement], 2.5, {bezier:{values:motionPathData, type:"cubic"}}, "-=2.5")
    } 

    getPathData(path){
        return MorphSVGPlugin.pathDataToBezier(path, { matrix: [0.60,0.00,0.00,0.60,0,0]});
    }

    animateELementToStartOfAnimatingPath(tl, path, ELementSelector){
        this.timeline.set([this.SELECTOR_HAND_IMAGE], {xPercent: this.pathData[0].x, yPercent: this.pathData[0].y});
        const pathData = this.getPathData(path);
        tl.set([ELementSelector], { x:pathData[0].x, y:pathData[0].y});
        
    }
	
}

const banner = new Banner();
