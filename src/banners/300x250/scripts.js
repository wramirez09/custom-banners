import BannerBase from '../../js/base';

class Banner extends BannerBase {
	constructor(paths) {
		super(paths);
		this.pathData = [
            
			{ path:"#path_1", x:65, y:24},
			{ path:"#path_2", x:65, y:24},
			{ path:"#path_3", x:65, y:24}
		];
    }
    
    followAnimatingPath(tl, selector_pathToFollow, selector_followingElement){
        const motionPathData = MorphSVGPlugin.pathDataToBezier(selector_pathToFollow, { align: selector_followingElement, matrix: [0.50,0.00,0.00,0.50,0,0]});

        tl.to([selector_followingElement], 2.5, {bezier:{values:motionPathData, type:"cubic"}}, "-=2.5")
    } 

    getPathData(path){
        return MorphSVGPlugin.pathDataToBezier(path, { matrix: [0.50,0.00,0.00,0.50,0,0]});
    }

    animateELementToStartOfAnimatingPath(tl, path, ELementSelector){
        this.timeline.set([this.SELECTOR_HAND_IMAGE], {xPercent: this.pathData[0].x, yPercent: this.pathData[0].y});
        const pathData = this.getPathData(path);
        tl.set([ELementSelector], { x:pathData[0].x, y:pathData[0].y});
        
    }


	step1(){
        this.dl_timeline.set([this.SELECTOR_HAND_IMAGE], {transformOrigin: "top left"})
        this.fadeInCopy(document.querySelectorAll('.banner__leadCopy1'));
             
        this.step2();   
        
    }

    step1() {

        this.fadeInCopy(document.querySelectorAll('.banner__leadCopy1'));
        this.step2();

    }



	step1(){


        this.fadeInCopy(document.querySelectorAll('.banner__leadCopy1'));
        this.step2();      
        
    }

    step2(){

        this.timeline.to(['.banner__enbrelLogo'],1, {delay: 0, opacity:1, onComplete:()=>{
            
        }}, "-=1")
        this.timeline.to(['.banner__leadCopy1'], 1 , {opacity:0, onComplete:()=>{
            
            
        }}, "-=1")
        this.timeline.to('.banner__leadCopy2', 1, {delay: 0, opacity:1, onComplete:()=>{

            
        }}, "-=1")
        
        this.timeline.to('#runningMan', 1, {opacity: 0, onComplete:()=>{
            
        }}, "-=1")
        this.timeline.to(["#isi-wrapper", ".banner__piLinks"], 1, {x:0, y:0, onComplete:()=>{
            
        }})// isi comes up
        this.animateELementsToJustAboveTheIsi('.banner__scissors', 35, "-=1");
        this.animateELementsToJustAboveTheIsi('.banner__tape', 50, "-=1"); 
        this.timeline.to('.banner__scissors', 1, {scale:.75, x:-5, y:-20, onComplete:()=>{
            
        }}, "-=1")
        this.timeline.to('.banner__tape', 1, {scale:.90, x:-35, y:-10, onComplete:()=>{
            
        }}, "-=1")
        this.timeline.to('.banner__enbrelLogo', 1, {scale: .4, x:'-18%', y:"-42%", onComplete:()=>{
            this.bg_tl.stop();
            
        }}, "-=1")
        this.timeline.to(['.banner__bg'], 1, {scale:1, onComplete:()=>{
       }}, "-=1");
        this.timeline.to('.banner__rulerAndChalk', 1, {opacity:1, onComplete:()=>{
            
        }}, "-=1")
        
        this.step3();
        
    }

    step3(){


        this.timeline.to('.banner__button', 1, {opacity: 1, onComplete:()=>{
        }}, "-=1") 
        this.timeline.to('.banner__rulerAndChalk', 1, {opacity:1, onComplete:()=>{


        }})
        
        
    }
}

const banner = new Banner();
