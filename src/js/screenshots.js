/*
 * Takes a screenshot when running in automatic screenshot mode with puppeteer
 *
 * @param bool [fullPage=false] If true the entire unrolled ISI is captured, if false then only the banner's dimensions are included
 *
 * @return Promise The promise returned by puppeteer's screenshot function.
 *
*/
function takeScreenshot(fullPage) {
	banner.timeline.pause();
	puppeteerScreenshot(fullPage)
	.then(function () {
		banner.timeline.play();
	});
}

/*
* These functions are injected by Puppeteer and can be used to control screenshot mode
* They can be removed from non screenshot builds using gulp remove code
* For example:
* //removeIf(!screenshot)
* puppeteerScreenshot(true);
* this.timeline.addCallback(puppeteerScreenshot);
* setTimeout(endPuppeteer, 1000);
* //endRemoveIf(!screenshot)
*/

/*
* Takes a screenshot when running in automatic screenshot mode with puppeteer
*
* @param bool [fullPage=false] If true the entire unrolled ISI is captured, if false then only the banner's dimensions are included
*
* @return Promise The promise returned by puppeteer's screenshot function.
*
*/
//puppeteerScreenshot(fullPage)

/*
 * Ends the current puppeteer task
 * As the screenshots take some time to complete be sure the final screenshot is finished before calling this using a delay or promises
 *
 */
//endPuppeteer()
