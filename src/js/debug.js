var debugPlayButton = document.getElementById('debug-controls-play');
var debugScrubber = document.getElementById('debug-controls-scrubber');
var playingBeforeScrub;

debugPlayButton.addEventListener('click', () => {
	if (banner.timeline.paused() || banner.timeline.totalProgress() == 1) {
		if (banner.timeline.totalProgress() == 1) {
			banner.timeline.totalProgress(0);
		}
		banner.timeline.play();
		debugPlayButton.innerHTML = "Pause";
	} else {
		banner.timeline.pause();
		debugPlayButton.innerHTML = "Play";
	}
});

debugScrubber.addEventListener('input', function () {
	banner.timeline.totalProgress(this.value);
});

debugScrubber.addEventListener('mousedown', function () {
	playingBeforeScrub = !banner.timeline.paused();
	if (playingBeforeScrub) {
		//Pause while scrubbing so it doesn't keep moving while the user drags
		banner.timeline.pause();
	}
});

debugScrubber.addEventListener('mouseup', function () {
	if (playingBeforeScrub) {
		//Resume playing if we were playing before we started scrubbing
		banner.timeline.play();
		//This is to handle the fringe case where user scrubs from a completed timeline
		debugPlayButton.innerHTML = "Pause";
	}
});


banner.timeline.eventCallback('onUpdate', function () {
	document.getElementById('debug-controls-scrubber').value = banner.timeline.totalProgress();
	var progress = (banner.timeline.duration() * banner.timeline.totalProgress()).toFixed(2);
	var duration = banner.timeline.duration().toFixed(2);
	document.getElementById('debug-controls-time-display').innerHTML = progress + '/' + duration;
});

banner.timeline.eventCallback('onComplete', function () {
	if (banner.timeline.duration() > 0) {
		debugPlayButton.innerHTML = "Play";
	}
});
