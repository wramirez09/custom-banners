import ISIScroller from './isiScroller';
export default class BannerBase {
	constructor(paths) {
		this.isiScroller = new ISIScroller();
		this.paths = paths;
		this.runAnimationInit = this.runAnimationInit.bind(this);
		this.animate = this.animate.bind(this);
		this.onLoad = this.onLoad.bind(this);
        this.timeline = new TimelineMax();
		window.onload = this.onLoad;
		/** animation stuff below */
		this.pathData = [
            
            { path:"#path_1", x:18, y:59},
            { path:"#path_2", x:3, y:48},
            { path:"#path_3", x:1, y:63}
        ];
        this.init = this.init.bind(this);       
        this.isFunction = this.isFunction.bind(this);
        this.animateLogoPath = this.animateLogoPath.bind(this);
        this.getPathData = this.getPathData.bind(this);
        this.followAnimatingPath = this.followAnimatingPath.bind(this);
        this.animateELementToStartOfAnimatingPath = this.animateELementToStartOfAnimatingPath.bind(this);
        this.SELECTOR_HAND_IMAGE = '.banner__handImage';
        this.step1.bind(this);
        this.step2.bind(this);
        this.step3.bind(this);
        this.bg_tl = new TimelineMax();
        this.dl_timeline = new TimelineMax();
        this.overLap = "-=3";
        this.fadeInCopy = this.fadeInCopy.bind(this);
        this.handImageWidth = document.querySelector(this.SELECTOR_HAND_IMAGE).clientWidth;
        this.animateELementsToJustAboveTheIsi = this.animateELementsToJustAboveTheIsi.bind(this);
        this.animateBg = this.animateBg.bind(this);
        this.runDottedLineAnimation = this.runDottedLineAnimation.bind(this);
        this.matrix = [];

	
	}

	onLoad() {
		if (Enabler.isInitialized()) {
			this.init();
		} else {
			Enabler.addEventListener(studio.events.StudioEvent.INIT, this.init);
		}
	}

	init() {
		if (Enabler.isPageLoaded()) {
			this.politeInit();
		} else {
			Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, this.politeInit);
        }

        document.getElementById('frame').addEventListener('click', () => {
            Enabler.exit("Background Exit", 'https://www.enbrelpro.com/clinical-data/efficacy-rheumatoid-arthritis');
        });

        document.querySelector('.banner__button').addEventListener('click', () => {
            Enabler.exit("cta Exit", 'https://www.enbrelpro.com/clinical-data/efficacy-rheumatoid-arthritis');
        });
    
        document.querySelector('.banner__enbrelLogo').addEventListener('click', (e) => {
            e.stopPropagation()
            Enabler.exit("Enbrel Logo Exit", 'https://www.enbrelpro.com/');
        });
        
        document.querySelector('.banner__piLinks-pi > a').addEventListener('click', (e) => {
            e.stopPropagation()
            Enabler.exit("PI Link Exit", 'https://www.pi.amgen.com/~/media/amgen/repositorysites/pi-amgen-com/enbrel/enbrel_pi.pdf');
        });

        document.querySelector('.isi-pi-link').addEventListener('click', (e) => {
            e.stopPropagation()
            Enabler.exit("PI Link Exit", 'https://www.pi.amgen.com/~/media/amgen/repositorysites/pi-amgen-com/enbrel/enbrel_pi.pdf');
        });
    
        document.querySelector('.banner__piLinks-medGuide > a').addEventListener('click', (e) => {
            e.stopPropagation()
            Enabler.exit("Med Guide Exit", 'https://www.pi.amgen.com/~/media/amgen/repositorysites/pi-amgen-com/enbrel/enbrel_mg.pdf');
        });

        document.querySelector('.isi-medguideLink').addEventListener('click', (e) => {
            e.stopPropagation()
            Enabler.exit("Med Guide Exit", 'https://www.pi.amgen.com/~/media/amgen/repositorysites/pi-amgen-com/enbrel/enbrel_mg.pdf');
        });
    
        document.querySelector('.banner__amgenLogo').addEventListener('click', (e) => {
            e.stopPropagation()
            Enabler.exit("Amgen Logo Exit", 'https://www.amgen.com/');
        });
    
        document.querySelector('.banner__enbrelLink').addEventListener('click', (e) => {
            e.stopPropagation()
            Enabler.exit("Enbrel Link Exit", 'https://www.enbrelpro.com/');
        });
		this.animate();
	}

	// Runs when the page is completely loaded.
	politeInit() {
		// Add your code to hide any loading image or animation and load creative
		// assets or begin creative animation.
	}

	animate() {

		this.runAnimationInit();
		// extend in campaign or banner

		//Ends screenshot task once final screenshots are completed
		//removeIf(!screenshot)
		//Commented out for now as this doesn't currently work. See the open ticket in Teams for more info
		//this.timeline.addCallback(endPuppeteer);
		//endRemoveIf(!screenshot)

		//Call this after timeline is finished
		//A delay can also be added
		this.timeline.addCallback(this.isiScroller.start);
	}
	
	isFunction(functionToCheck){
        return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
    }

    animateLogoPath(tl, maskSelector, cb){

        const pathLength = document.querySelector(maskSelector).getTotalLength();

        tl.fromTo([maskSelector], 2.5, {drawSVG:0}, {drawSVG:pathLength});
       
        if(cb && this.isFunction(cb)){
            cb();
        }
       
    }
    
    followAnimatingPath(tl, selector_pathToFollow, selector_followingElement){
        const motionPathData = MorphSVGPlugin.pathDataToBezier(selector_pathToFollow, { align: selector_followingElement, matrix: [0.25,0.00,0.00,0.25,0,0]});

        tl.to([selector_followingElement], 2.5, {bezier:{values:motionPathData, type:"cubic"}}, "-=2.5")
    }

    getPathData(path){
        return MorphSVGPlugin.pathDataToBezier(path, { matrix: [0.25,0.00,0.00,0.25,0,0]});
    }

    animateELementToStartOfAnimatingPath(tl, path, ELementSelector, iteration){

        this.timeline.set([this.SELECTOR_HAND_IMAGE], {xPercent: this.pathData[0].x, yPercent: this.pathData[0].y});
        const pathData = this.getPathData(path);
        tl.set([ELementSelector], { x:pathData[0].x, y:pathData[0].y});
        
    }

    fadeInCopy(selectors){
        const timeline2 = new TimelineMax();
        [].forEach.call(selectors, (selector, i)=>{
            timeline2.to([selector], .75, {delay: .333*1, opacity: 1});
        })
    }

    animateELementsToJustAboveTheIsi(selector, offset, overLap){
        const height = document.querySelector(selector).offsetHeight;
        const frameHeight = document.querySelector('#frame').offsetHeight;
        this.timeline.to([selector], .75, {top:0 + frameHeight - height +  offset}, overLap);
    }

    runDottedLineAnimation(){

        for(var i = 0; i < this.pathData.length; i++) {

			this.animateELementToStartOfAnimatingPath(this.timeline, this.pathData[i].path + '_mask', this.SELECTOR_HAND_IMAGE, i);
			this.animateLogoPath(this.timeline, this.pathData[i].path + '_mask', null);
            this.followAnimatingPath(this.timeline, this.pathData[i].path, this.SELECTOR_HAND_IMAGE, this.overLap);

            if(i < this.pathData.length - 1) {
                const nextPathData = this.getPathData(this.pathData[i + 1].path + "_mask");
                this.timeline.to([this.SELECTOR_HAND_IMAGE], 0.25, { scale: 1.10 });
                this.timeline.to([this.SELECTOR_HAND_IMAGE], 0.5, { x:nextPathData[i].x, y:nextPathData[i].y});
                this.timeline.to([this.SELECTOR_HAND_IMAGE], 0.25, { scale: 1 });
            }

            
            

        }

        this.timeline.to([this.SELECTOR_HAND_IMAGE], 1, {x:this.handImageWidth + 35, y:this.handImageWidth+ 35});        
    
    }
    animateBg(){
        
        this.bg_tl.to(['.banner__bg'], 1, {scale:1, onComplete:()=>{
  
        }});
    }

    runAnimationInit() {
       
        this.animateELementToStartOfAnimatingPath(this.timeline, "#path_1_mask", this.SELECTOR_HAND_IMAGE);
        this.timeline.to('.frame', .75, {opacity:1, onComplete:()=>{
        }});
        this.runDottedLineAnimation(this.paths);

        this.step1();
        

    } 

    step1(){
        this.fadeInCopy(document.querySelectorAll('.banner__leadCopy1'));
        this.step2();        

    }

    step2(){
        
        this.timeline.to(['.banner__leadCopy1'], 1 , {delay: 1, opacity:0, onComplete:()=>{
            
            }})
        this.timeline.to('.banner__leadCopy2', 1, {delay: 0, opacity:1, onComplete:()=>{
            
            }}, "-=1")
        this.timeline.to(["#isi-wrapper", ".banner__piLinks"], 1, {x:0, y:0, onComplete:()=>{
            }}, "-=1")// isi comes up
        this.timeline.to(['.banner__enbrelLogo'],1, { opacity:1, onComplete:()=>{
            }}, "-=1")

        this.animateELementsToJustAboveTheIsi('.banner__scissors', 35, "-=1");
        this.animateELementsToJustAboveTheIsi('.banner__tape', 50, "-=1"); 

        this.timeline.to('.banner__scissors', 1, {scale:.55, x:-45, y:-20, onComplete:()=>{
            }}, "-=1")
        this.timeline.to('.banner__tape', 1, {scale:.90, x:-35, y:-10, onComplete:()=>{
            }}, "-=1")
        this.timeline.to('.banner__enbrelLogo', 1, {scale: .6, x:'-8%', top:"47%", onComplete:()=>{
            
            }}, "-=1")
        this.timeline.to(['.banner__bg'], 1, {scale:1}, "-=1");
        this.timeline.to('#runningMan', 1, { scale:.6, x:'-53%', top:"37%", onComplete:()=>{
            }}, "-=1")
        this.timeline.to('#runningMan', 1, {opacity: 0, onComplete:()=>{
            }}, "-=1")
        this.timeline.to('.banner__rulerAndChalk', 1, {opacity:1, onComplete:()=>{
        }}, "-=1")
        
        this.step3();
        
    }

    step3(){

        this.timeline.to('.banner__button', 1, {opacity: 1, onComplete:()=>{
        }}, "-=1")
        this.timeline.to('.banner__rulerAndChalk', 1, {opacity:1, onComplete:()=>{
           
    }});
        
        
    }

}
