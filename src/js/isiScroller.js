export default class ISIScroller {
	constructor() {
		this.isiScrolled = false;
		this.isiWrapper = document.getElementById('isi-wrapper');
		this.isiContent = document.getElementById('isi-content');
		this.scrollAnimation = null;

		this.start = this.start.bind(this);
		this.reset = this.reset.bind(this);
		this.stopScrolling = this.stopScrolling.bind(this);

		this.isiWrapper.addEventListener('wheel', this.stopScrolling);
		this.isiWrapper.addEventListener('mousedown', this.stopScrolling);
		this.isiWrapper.addEventListener('touchstart', this.stopScrolling);
		this.isiWrapper.addEventListener('click', (e) => {
			e.stopPropagation();
		});
	}

	start() {
		if (!this.isiScrolled && this.isiWrapper && this.isiContent && this.scrollAnimation === null) {
			const distance = this.isiContent.offsetHeight;
			const duration = (distance - this.isiWrapper.scrollTop) / 15;
			this.scrollAnimation = TweenMax.to(this.isiWrapper, duration, {
				scrollTo: { y: distance },
				ease: Power0.easeNone,
				onComplete: () => {
					this.scrollAnimation = null;
					this.isiScrolled = true;
				}
			});
		}
	}

	reset() {
		if (this.scrollAnimation) {
			this.scrollAnimation.kill();
			this.scrollAnimation = null;
		}
		this.isiWrapper.scrollTop = 0;
		this.isiScrolled = false;
	}

	stopScrolling() {
		if (this.scrollAnimation) {
			this.scrollAnimation.kill();
			this.scrollAnimation = null;
		}
		this.isiScrolled = true;
	}
}
