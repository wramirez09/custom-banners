var simple_time = {
    'begin' : 0,
    'end'   : 0,
    'total' : null,
    'times' : [ 0, 0, 0 ],
    'running': false,
    'start' : function() {
        if (!this.running) {
            this.running = true;
            this.begin = performance.now();
            console.log("Timer started");
        }
    },
    'stop'   : function() {
        if (this.running) {
            this.running = false;
            this.end = performance.now();
            console.log("Timer ended: " + this.calc());
            return this.calc();
        }
    },
    'calc'  : function() {
        var diff = this.end - this.begin;
        this.times[2] = diff / 10;
        if (this.times[2] >= 100) {
            this.times[1] = Math.floor(this.times[2] / 100);
            this.times[2] -= this.times[1] * 100;
        }
        if (this.times[1] >= 60) {
            this.times[0] = Math.floor(this.times[1] / 60);
            this.times[1] -= this.times[0] * 60;
        }
        return this.format();
    },
	'pad'   : function(number, length) {
    	var str = '' + number;
    	while (str.length < length) { str = '0' + str; }
   	    return str;
	},
    'format': function () {
        return ( this.pad(this.times[0], 2) + ":" + this.pad(this.times[1], 2) + ":" + this.pad(Math.floor(this.times[2]), 2) );
    }
};

export default simple_time;