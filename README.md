
# 042_58558_RA_Customizable_Banner_Ads

This project was generated using the [AT Banner Generator](https://bitbucket.org/abelsontaylor/banner-generator/src/master/)

## Requirements

- [Node.js](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/en/)
- [Visual Studio Code](https://code.visualstudio.com)

## Installation

Yeoman ran yarn automatically to install required packages when this project was created. However, if this project is being checked out to a different location you will need to manually install packages using

`yarn install`

## Usage

Build scripts/tasks can be run through the different options listed below. For users unfamiliar with this setup, the Visual Studio Code configurations will be the easiest to identify and use.

### Visual Studio Code configurations

The most common tasks can be accessed through the dropdown menu in the debug tab in Visual Studio Code. These are configured in the .vscode/launch.json file to run gulp tasks with node environment variables.

#### development

This will clean the dist folder and run a gulp development build.

#### production

This will clean the dist folder and run a gulp production build. Run this before uploading/distributing/packaging.

#### adchoice

This will clean the dist folder and run a gulp production build with the addition of the AdChoice logo.

#### screenshot

This will clean the dist folder and run a gulp production build. It will then run the screenshot task which uses puppeteer to automatically run the banners and generate screenshots.

### yarn scripts

These are shortcuts for gulp tasks that can be run using yarn (e.g. yarn build)

There are also scripts that mirror the Visual Studio Code configurations (e.g. production).

They are below and in the package.json file.

#### yarn clean

Delete the dist folder.

#### yarn build

Runs a development build without cleaning the dist folder.

#### yarn watch

Runs a development build and starts watching the files for changes in order to rebuild when needed.

#### yarn development

This will clean the dist folder and run a gulp development build.

#### yarn production

This will clean the dist folder and run a gulp production build. Run this before uploading/distributing/packaging.

#### yarn adchoice

This will clean the dist folder and run a gulp production build with the addition of the AdChoice logo.

#### yarn screenshot

This will clean the dist folder and run a gulp production build. It will then run the screenshot task which uses puppeteer to automatically run the banners and generate screenshots.

### Gulp tasks

These are the underlying asks that are run by Visual Studio Code or yarn.

#### npx gulp clean

Delete the dist folder.

#### npx gulp build

Runs a development build without cleaning the dist folder.

#### npx gulp watch

Runs a development build and starts watching the files for changes in order to rebuild when needed.
