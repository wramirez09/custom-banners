const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const path = require('path');
const promisedDel = require('promised-del');
const postcssCombineSelectors = require('postcss-combine-duplicated-selectors');
const postcssImport = require('postcss-import');
const puppeteer = require('puppeteer');
const rollup = require('gulp-better-rollup');
const rollupBabel = require('rollup-plugin-babel');
const inline = require('gulp-inline')
const removeCode = require('gulp-remove-code');
var fs = require('fs-extra');

const $ = gulpLoadPlugins();

const development = $.environments.development;
const production = $.environments.production;

const clean = () => promisedDel('dist');
const cleanTemp = () => promisedDel('temp');
const cleanCSS = () => promisedDel('dist/**/*.css');
const cleanHTML = () => promisedDel('dist/**/*.html');
const cleanJS = () => promisedDel('dist/**/*.js');
const cleanImages = () => promisedDel('dist/**/*.{jpg,png}');
const cleanScreenshots = () => promisedDel('screenshots');

const css = () =>
	gulp.src('**/banners/**/*.scss', { base: 'src/banners' })
		.pipe($.tap((file) => {
			const folderName = path.basename(path.dirname(file.path));
			const sizes = folderName.split('x');
			file.contents = new Buffer.from(('$environment:'+(development()?'development':'production')+';\n$width: ' + sizes[0] + 'px;\n$height: ' + sizes[1] + 'px;\n\n').concat(String(file.contents)));
		}))
		.pipe(development($.sourcemaps.init()))
		.pipe($.sass({
			indentType: 'space',
			indentWidth: 2
		}).on('error', $.sass.logError))
		.pipe($.postcss([
			postcssImport(),
			postcssCombineSelectors({ removeDuplicatedProperties: true }),
			autoprefixer({ grid: true }),
			cssnano({ autoprefixer: false })
		]))
		.pipe($.rename((path) => {
			path.basename += '.min'
		}))
		.pipe(development($.sourcemaps.write('.')))
		.pipe($.eol())
		.pipe(gulp.dest('dist'));

const html = () =>
	gulp.src('**/banners/**/*.pug', { base: 'src/banners' })
		.pipe($.data((file) => {
			const folderName = path.basename(path.dirname(file.path));
			const sizes = folderName.split('x');

			return {
				'width': sizes[0], 'height': sizes[1],
				'title': folderName,
				'environment': development() ? 'development' : 'production',
				'adchoice' : process.env.adchoice === 'true' ? 'true' : 'false',
				'screenshot' : process.env.screenshot === 'true' ? 'true' : 'false'
			};
		}))
		.pipe($.pug())
		.pipe(inline({
			base: 'src/',
			svg: $.svgmin,
			disabledTypes: ['css', 'img', 'js'] // Only inline svg files
		}))
		.pipe(development($.htmlBeautify({
			'indent_size': 1,
			'indent_char': '	',
			'wrap_line_length': 0,
			'end_with_newline': true
		})))
		.pipe(production($.htmlmin({ collapseWhitespace: true })))
		.pipe(production($.eol()))
		.pipe(gulp.dest('dist'));

const debugTools = () =>
	gulp.src('src/js/debug.js', { base: 'src' })
		.pipe(development(gulp.dest('dist')));

const jsTemp = () =>
	gulp.src('src/**/*.js',  { base: 'src' })
		.pipe(removeCode({ screenshot: process.env.screenshot == 'true' }))
		.pipe(gulp.dest('temp'));

const jsRollup = () =>
	gulp.src('temp/**/banners/**/*.js', { base: 'temp/banners' })
		.pipe(development($.sourcemaps.init()))
		.pipe(rollup({
			plugins: [rollupBabel()]
		}, {
			format: 'cjs'
		}))
		.pipe($.rename((path) => {
			path.basename += '.min'
		}))
		.pipe(development($.sourcemaps.write('.')))
		.pipe(production($.eol()))
		.pipe(gulp.dest('dist'));

const js = gulp.series(debugTools, jsTemp, jsRollup);

const copyVendorJs = async () => {

	fs.readdir('./dist/', (err, folders) => {
		folders.forEach((folder, i) => {
			fs.copy('./src/js/client-vendor/', `./dist/${folder}/`)
				.then(() => console.log('success!'))
				.catch(err => console.error(err));
		});
	})
} 

const copySvg = async () =>{
	gulp.src('./src/banners/**/*.svg').pipe(gulp.dest('dist'))

}

const images = () =>
	gulp.src('**/banners/**/*.{jpg,png}', { base: 'src/banners' })
		.pipe(gulp.dest('dist'));

const build = gulp.series(gulp.parallel(css, html, debugTools, js, images), copyVendorJs);

const defaultTask = gulp.series(gulp.parallel(css, html, debugTools, js, images, copySvg), copyVendorJs);

const watch = () => {
	gulp.watch('src/**/*.scss', gulp.series(cleanCSS, css));
	gulp.watch('src/**/*.{pug,svg}', gulp.series(cleanHTML, html));
	gulp.watch('src/**/*.js', gulp.series(cleanJS, js, cleanTemp, copyVendorJs));
	gulp.watch('src/**/*.{jpg,png}', gulp.series(cleanImages, images));
	copyVendorJs();
};

gulp.task('screenshots', async () => {
	await fs.mkdir('./screenshots', { recursive: true }, (err) => {
		if (err) throw err;
	});
	gulp.src('src/js/screenshots.js', { base: 'src' })
        .pipe(gulp.dest('dist'));
	gulp.src('dist/**/*.html')
		.pipe($.tap((file) => {
			const folderName = path.basename(path.dirname(file.path));
			const filepath = './screenshots/' + folderName + '-';
			const sizes = folderName.split('x');
			const filetype = '.png';
			var i = 0;
			const reset = () => {
				document.getElementById('reset').click();
			};
			(async (file) => {
				function takeScreenshot(fullPage) {
					page.evaluate(fullPage => {
						if (fullPage) {
							document.getElementById('banner').classList.add('unroll-isi');
						} else {
							document.getElementById('banner').classList.remove('unroll-isi');
						}
					}, fullPage);
					return page.screenshot({
						path: filepath + (i++) + filetype,
						fullPage: fullPage
					});
				}
				const browser = await puppeteer.launch({headless: false});
				const page = await browser.newPage();
				await page.exposeFunction("puppeteerScreenshot", async (fullPage) => {
					return takeScreenshot(fullPage);
				});
				await page.setViewport({ width: parseInt(sizes[0], 10), height: parseInt(sizes[1], 10) });
				await page.goto('file://' + file.path.toString());
				await takeScreenshot(true);
				await page.exposeFunction("puppeteerExit", () => {
					return browser.close();
				});
			})(file);
		}))
});

exports.clean = clean;
exports.build = gulp.series(build, cleanTemp);
exports.watch = watch;
exports.screenshot = gulp.series(clean, cleanScreenshots, build, cleanTemp, 'screenshots');
exports.default = gulp.series(clean, defaultTask, cleanTemp);
exports.copyVendorJs = copyVendorJs;
